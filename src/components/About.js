import {Row, Col} from 'react-bootstrap';

export default function About(){

	return (

		<Row>
			<Col className="p-5 bg-danger">
				<h1 className="my-5">About Me</h1>
				<h2 className="mt-3">Tee Jae Calinao</h2>
				<h3>Full Stack Web Developer</h3>
				<p className="mb-4">I am a Full Stack Web Developer from Zuitt Coding Bootcamp. I have been a professional teacher for 9 years. I am a career-shifter.</p>
				<h4>Contact Me</h4>
				<ul>
					<li>Email: teejae.calinao@tuitt.com</li>
					<li>Mobile No: 09266772411</li>
					<li>Address: Cainta, Rizal, Philippines 1900</li>
				</ul>
			</Col>
		</Row>

	)

}