//import react bootstrap components
//react-bootstrap components are react components that create/return react elements with the appropriate bootstrap classes.
import React from 'react';
import {Button, Row, Col} from 'react-bootstrap'

//destructure the special object received and get only the props we need.
export default function Banner({bannerProp}){

	console.log(bannerProp);

	/*
		All components which are children to other components can receive a special object. We can receive it here as a parameter.

	*/

	/*To be able to add a class in your react elements and react bootstrap components, we add className, instead of class*/

	/*
		Mini-Activity:

		Display the description and buttonText from our props as the text content for the paragraph and the button.

		Take a screenshot of our banner and send it to the hangouts.

	*/

	return (

		<Row>
			<Col className="p-5">
				<h1 className="mb-3">{bannerProp.title}</h1>
				<p className="my-3">{bannerProp.description}</p>
				<a href={bannerProp.destination} className="btn btn-primary">{bannerProp.buttonText}</a>
			</Col>
		</Row>

	)

}