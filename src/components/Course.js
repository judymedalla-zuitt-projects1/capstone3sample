//import useState from react if you're going to use states in a component.
import React, {useState} from 'react';

import {Card,Button} from 'react-bootstrap';

//import Link component - Link component is a component from react-router-dom so that we will simply switch the pages and not refresh the page when link to a different page.
import {Link} from 'react-router-dom'

export default function CourseCard({courseProp}){

	//console.log(courseProp);

	//console.log("Hello, I will run whenever we update a state with its setter function.")

	//create your states using useState and keep in mind to add them INSIDE the component and at the top of the component itself.

	const [count,setCount] = useState(0);
	const [seats,setSeats] = useState(30);

	/*
		Mini-Activity:

		Create a new state using the useState() react hook.

			-The initial value of the state should be 30.
			-the name of the state should be seats.
			-the name of the state's setter function should be setSeats.

		In our enroll function, update the seats state with its setSeats() setter function by subtracting 1 from the state.

		Embed the value of seats state in our return.

		Take a screenshot of your page after clicking enroll button once.

		Send your screenshot in the hangouts.



	*/

	//useState react hook returns an array which contains the state and the setter function.

	//count is our state and was destructured from our useState() and its initial value is the argument added to useState(<initialValue>)

	//setCount() is the setter function for the state. We use this setter function to update the state and in every of update of the state, the component will re-render.

	//rendering - is when we display our react elements from a component.
	//re-rendering - is when we run our component again to display our react elements. When a component re-renders, the components run again from the start.

	//console.log(count);

	//useState() returns an array.
	//console.log(useState("Hello"));

	/*States*/
	/*
		States in Reactjs are ways to store information within a component. The advantage of a state from a variable is that especially within a component, variables do not retain updated information when the component is updated or re-rendered because the variable will reset to its initial value whenever the component re-rendered.

		Creating a state:

		useState() hook from react will allow us to create a state and its setter function.

	*/

	//sample seat variable
	//let seat = 0;

	//We will attach the enroll function on a click event we will add on our button.
	//Whenever the button is clicked, the enroll function will run and the count state will be updated by the setter function.
	function enroll(){

		setCount(count + 1);
		setSeats(seats - 1);
		//seat++

	}

	//console.log(seat)

	/*
		Conditional rendering is the ability to display or hide elements based on a condition. We can embed ternary operator in our react elements. We cannot however add/embed an if-else statement in a react element.

	*/

	/*
		We will pass the id of the course in the Link component, we will pass the id of the course in the browser URL as params.

	*/

	//console.log(courseProp);

	return (
			<Card>
				<Card.Body>
					<Card.Title>
						{courseProp.name}
					</Card.Title>
					<Card.Text>
						{courseProp.description}
					</Card.Text>
					<Card.Text>
						Price: {courseProp.price}
					</Card.Text>
					<Link to={`/courses/viewCourse/${courseProp._id}`} className="btn btn-primary">View Course</Link>
				</Card.Body>
			</Card>
		)

}


/*
	Mini-activity:

	Display the description and price of our course from your courseProp.

	Take a screenshot of your page and the first CourseCard Component and send it in the hangouts.

*/