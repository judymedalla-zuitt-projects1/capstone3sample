//mockdata for courses
let coursesData = [
	
	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Lorem ipsum officia reprehenderit cillum voluptate in sit proident dolor sint voluptate deserunt.",
		price: 25000,
		onOffer:true
	},
	{
		id: "wdc002",
		name: "Python-Django",
		description: "Enim exercitation dolore eu ullamco sit ut et labore aliqua culpa ut dolor ut aliqua deserunt cillum sint nisi in occaecat incididunt aliquip non cillum id esse et ad proident eiusmod in ut.",
		price: 35000,
		onOffer:true
	},
	{
		id: "wdc003",
		name: "Java-Springboot",
		description: "Anim dolore mollit nisi dolore deserunt aliqua sint esse voluptate qui reprehenderit sint laboris veniam duis occaecat deserunt.",
		price: 45000,
		onOffer:true
	},
	{
		id: "wdc004",
		name: "Node.js-ExpressJS",
		description: "Cillum incididunt aliqua elit nisi consequat consequat dolore occaecat eu incididunt anim sed pariatur elit velit voluptate ea voluptate.",
		price: 45000,
		onOffer: false
	}
	
];

export default coursesData;