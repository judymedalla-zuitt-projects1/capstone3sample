import React, {useState,useContext,useEffect} from 'react';
import {Form,Button} from 'react-bootstrap';

import Swal from 'sweetalert2';

import UserContext from '../userContext';

//import Navigate component from react-router-dom, this will allow us to Redirect our users after logging in and updating the global user state. If a logged in user tries to go to our login page via browser, they will be redirected to our home page.
import {Navigate} from 'react-router-dom';

export default function Login(){

	//unwrap our UserContext and get our global user state and its setter function
	const {user,setUser} = useContext(UserContext);

	//console.log(user);
	//console.log(setUser);

	//input states
	const [email,setEmail] = useState("");
	const [password,setPassword] = useState("");

	//state for conditional rendering our button
	const [isActive,setIsActive] = useState(false);
	//false is the initial value because initially, the form is empty and the button must be disabled.

	//add useEffect to check if the user is able to fill up our form. As long as both inputs are not filled, the submit button is disabled, else, it will be enabled.
	//this useEffect() will run when the states in the dependency array are updated.
	useEffect(()=>{

		if(email !== "" && password !== ""){
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	},[email,password])

	function loginUser(e){

		//prevent submit event's default behavior
		e.preventDefault();

		fetch(' https://enigmatic-sands-29458.herokuapp.com/api/users/login',{

			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({

				email: email,
				password: password

			})
		})
		.then(res => res.json())
		.then(data => {

			//data is the response of the api/server after it's been process as JS object through our res.json() method.
			//console.log(data);
			//data will only contain an email property if we can properly logged in our user.
			if(data.accessToken){

				Swal.fire({

					icon:"success",
					title: "Login Successful",
					text: "Thank you for logging in!"

				})

				//save our accessToken when we log in successfully. We will save it in our localStorage.
				//localStorage is an object in JS which allows us to save small amounts of data within our browser. We can use this to save our token.
				//localStorage exists in most browsers.
				//localStorage.setItem() will allow us to save data in our browsers. However, any data we pass to localStorage will bcome a string.
				//syntax: localStorage.setItem(<key>,<value>)
				localStorage.setItem('token',data.accessToken);
				//localStorage.setItem('sample',"sample message");

				//localStorage.getItem(<token>) - allows us to get the data of the key we will pass from our localStorage.
				let token = localStorage.getItem('token');
				console.log(token);

				//use fetch() method to create a request to get our user details
				fetch(' https://enigmatic-sands-29458.herokuapp.com/api/users/details',{

					method: 'GET',
					headers: {
						//Authorization headers used for passing a token.
						'Authorization': `Bearer ${token}`
					}

				})
				.then(res => res.json())
				.then(data => {

					console.log(data);

					//localStorage.setItem('id',data._id);
					//localStorage.setItem('isAdmin',data.isAdmin);

					//update the global user state with the id and isAdmin details of our user.
					setUser({

						id: data._id,
						isAdmin: data.isAdmin

					})

				})


			} else {
				Swal.fire({

					icon: "error",
					title: "Login Failed",
					text: data.message

				})
			}

		})
	}

	//create a ternary to redirect our user if they are logged in. If not, we will show our form.
	return (

		user.id 
		?
		<Navigate to="/courses" replace={true} />
		:
		<React.Fragment>
			<h1 className="my-5 text-center">Login</h1>
			<Form onSubmit={e => loginUser(e)}>
				<Form.Group>
					<Form.Label>Email:</Form.Label>
					<Form.Control type="email" placeholder="Enter Email" required value={email} onChange={e => {setEmail(e.target.value)}}/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password:</Form.Label>
					<Form.Control type="password" placeholder="Enter Password" required value={password} onChange={e => {setPassword(e.target.value)}}/>
				</Form.Group>
				{
					isActive
					? <Button variant="primary" type="submit" className="my-5">Submit</Button>
					: <Button variant="primary" disabled className="my-5">Submit</Button>
				}
			</Form>
		</React.Fragment>

	)


}