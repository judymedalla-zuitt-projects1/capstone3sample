//Our home page component will be the parent component of our banner and highlights components.
import React from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home(){

	/*

		ReactJS adheres to the concept of D.R.Y. - Don't Repeat Yourself.

		Components in ReactJS are independent and reusable.

		What makes a ReactJS component reusable is with the use of props.

		Props are data we can pass from a parent component to a child component.

		Parent components are components which return other components.

		Child components are components returned by a parent component.

	*/

	//let sampleProp = "I am sample data passed from Home component to Banner Component."

	/*
		To pass prop from a parent component to a child component, we add HTML-like attribute to the child component which we can name ourselves. Props are html-like attributes we can name ourselves.

		The name of the attribute will become the name of a property of the object that the child component receives. That is why this is called Props. Props stand for properties.

	*/

	/*
		Mini-Activity:

		Create a new variable called sampleProp2.
			-This variable should contain a string with message:
				"This sample data is passed from Home to Highlights component."

		Pass the variable as a props from our Home component to our highlights component.

		Then in the Highlights component, destructure the incoming special react object and get only the props that contain our message.

		Log the prop in the console.

		Take a screenshot of your console and send it to the hangouts.

	*/

	let sampleProp2 = "This sample data is passed from Home to Highlights component."

	//object to be passed as props

	let bannerData = {

		title: "Zuitt Booking System",
		description: "View and book a course from our catalog!",
		buttonText: "View Our Courses",
		destination: "/courses"

	}


	return (

		<>
			<Banner bannerProp={bannerData}/>
			<Highlights highlightsProp={sampleProp2}/>
		</>

	)

}