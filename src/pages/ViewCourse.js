import React, {useState,useEffect,useContext} from 'react';
import {Card,Button,Row,Col} from 'react-bootstrap';
import {useParams,Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../userContext'

export default function	ViewCourse(){

	/*fetch the details of our course by its id.*/

	//useParams from react-router-rom will allow us to retrieve the data from our Browser URL.
	//useParams() will return an object containing URL params.
	//console.log(useParams());
	//destructure the object from useParams() and save it in a variable:
	const {courseId} = useParams();

	//console.log(courseId);

	//get the global user state from our context:
	const {user} = useContext(UserContext);
	console.log(user);

	//state to save our course details.
	const [courseDetails,setCourseDetails] = useState({

		name: null,
		description: null,
		price: null
	})


	//useEffect to get course details
	useEffect(()=>{

		//fetch to get our course details:
		fetch(` https://enigmatic-sands-29458.herokuapp.com/api/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {

			//console.log(data);
			setCourseDetails({

				name: data.name,
				description: data.description,
				price: data.price

			})

		})


	},[courseId])


	//enroll function will be run when logged in user clicks on the enroll button
	function enroll(){
		console.log("enroll");
		console.log(courseId);

		//fetch for enroll:
			//needs the courseId to be passed in the body.
			//to pass the token as part of the Authorization.
			/*
				Activity:

				Create a fetch() in enroll() in the ViewCourse() component which will allow us to enroll to our current course:
					-Provide the correct Request URL
					-Provide the appropriate Method
					-Provide the token
					-Provide the proper headers.
					-Provide the appropriate request body.
						fetch for enroll:
							needs the courseId to be passed in the body.
							to pass the token as part of the Authorization headers.

				Then, process the incoming response with res.json()
				Then, console.log() the incoming data.
			*/

		//add fetch for enroll
			fetch("https://enigmatic-sands-29458.herokuapp.com/api/users/enroll", {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${ localStorage.getItem('token') }`
				},
				body: JSON.stringify({
					courseId: courseId
				})
			})
			.then(res => res.json())
			.then(data => {

				console.log(data);
				if(data){
					Swal.fire({

						icon: "success",
						title: "Enrolled Successully.",
						text: "Thank you for enrolling."

					})
				} else {
					Swal.fire({

						icon: "error",
						title: "Enrollment Failed.",
						text: data.message

					})
				}

		})

	}

	return (


			<Row className="mt-5">
				<Col>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{courseDetails.name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{courseDetails.description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>{courseDetails.price}</Card.Text>
						</Card.Body>
						{
							user.id && user.isAdmin === false
							? <Button variant="primary" className="btn-block" onClick={enroll}>Enroll</Button>
							: <Link className="btn btn-danger btn-block" to="/login">Login To Enroll</Link>
						}
					</Card>
				</Col>
			</Row>

		)


}