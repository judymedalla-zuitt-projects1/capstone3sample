Activity 1

Create a new file called About.js in the components folder.
	-Create a new component called About() in the About.js file.
	-Return a component with:
		a Row
			a Col
				-add p-5 class

				Inside the col:
				an h1 containing the heading: About Me
					-add a my-5 class
				an h2 containing your name.
					-add mt-3 class
				an h3 containing the title: Full Stack Web Developer.
				an p containing a short description about yourself.
					-add mb-4 class
				an h4 containing: Contact Me
				a ul containing a list of your contact details.
					-email
					-mobile
					-address

	-Import the component in App.js and add it in the return after Banner component.

Pushing Instructions:

Go to Gitlab:
	-in your zuitt-projects folder and access b152 folder.
	-inside your b152 folder create a new repo called react-booking
	-untick the readme option
	-copy the git url from the clone button of your react-booking repo.

Go to Gitbash:
	-go to your b152/react-booking folder.
	-connect your local repo to our online repo: git remote add origin <gitURLOfOnlineRepo>
	-add your updates to be committed: git add .
	-commit your changes to be pushed: git commit -m "includes s43 reactjs Activity 1"
	-push your updates to your online repo: git push origin master

Go to Boodle:
	-copy the url of the home page for your react-booking repo (URL on browser not the URL from clone button) and link it to boodle:

	WDC028-43 | React.js - Intro


Activity 2

Create a new file called Course.js in the components folder.
	-Create a new component called CourseCard() in the Course.js file.
	-Return a component with:
		-Create a simple "Card" component containing hard coded values of a course.
			-Show the title of the course
			-Show the description of course
			-Show the price of the course
			-React-bootstrap button for enrollment.

	Stretch Goals: 
		Create 2 more CourseCard() components.
		
	-Import the component in App.js and render it after the Home page.			

Pushing Instructions:

Go to Gitlab:
	-in your zuitt-projects folder and access b152 folder.
	-inside your b152 folder create a new repo called react-booking
	-untick the readme option
	-copy the git url from the clone button of your react-booking repo.

Go to Gitbash:
	-go to your b152/react-booking folder.
	-connect your local repo to our online repo: git remote add origin <gitURLOfOnlineRepo>
	-add your updates to be committed: git add .
	-commit your changes to be pushed: git commit -m "includes s44 reactjs Activity 2"
	-push your updates to your online repo: git push origin master

Go to Boodle:
	-copy the url of the home page for your react-booking repo (URL on browser not the URL from clone button) and link it to boodle:

	WDC028-44 | React.js - JSX/Components

Activity 3:	

	In Course.js:

	1. Add a ternary operator that 
		If the seats state value finally hits zero, show a span with red colored text: "No More Seats Available."
		Else, show a span with green colored text and show the current value of seats.

	2. If the seats state value finally hits zero, show a button that is disabled or greyed-out and has no onClick event anymore. 
	   Else, show a button that is accessible and is working as intended.


Pushing Instructions:

Go to Gitbash:
	-go to your b152/react-booking folder.
	-add your updates to be committed: git add .
	-commit your changes to be pushed: git commit -m "includes s45 reactjs Activity 3"
	-push your updates to your online repo: git push origin master

Go to Boodle:
	-copy the url of the home page for your react-booking repo (URL on browser not the URL from clone button) and link it to boodle:

	WDC028-45 | React.js - Props and States

/*
	Activity:

	Create a login page similar to our register page.

	Create a form for the email and password.
	Create input states for your email and password.
		-Bind our input states per input element.
		-Add onChange event on the input elements to update the states with the input value.
	Create a loginUser function which is triggered by a submit event attached to our Form.
		-In this function, prevent the submit event's default behavior.
		-Create a fetch() which will send a request to our api for login.
			-Add the appropriate method,headers and body.
			-Then process the response with res.json()
			-Then, console log the incoming data.
				-Add an if-else statement which will show an alert:
					Show a sweetalert with success icon if data has an accessToken property.

					Else, show a sweetalert with an error icon if what was returned is otherwise.

	Pushing Instructions:

	Go to Gitbash:
		-go to your b152/react-booking folder.
		-add your updates to be committed: git add .
		-commit your changes to be pushed: git commit -m "includes s46 reactjs Activity 4"
		-push your updates to your online repo: git push origin master

	Go to Boodle:
		-copy the url of the home page for your react-booking repo (URL on browser not the URL from clone button) and link it to boodle:

		WDC028-46 | React.js - Effects, Events and Forms


Activity:

Create an AddCourse.js in the pages folder.
Create a new page component called AddCourse().

	-Create states for inputs:
		-name
		-description
		-price
	-Create a form to input course details


	-Create a new function called createCourse() which will be run when the user submits the form. 
		-Create a fetch() request to be able to send our data to our api and create a new course.
			-You're going to have to pass a token and request body.
			-Then, parse/process the incoming response using res.json()
			-Then, show the created course in the console and show a sweetalert for successful course creation.

	-Create a new route in the App.js and assign the AddCourse component to a path called "/addCourse".
	
	Note: Token must come from an admin. Login your admin user.



	Pushing Instructions:

	Go to Gitbash:
		-go to your b152/react-booking folder.
		-add your updates to be committed: git add .
		-commit your changes to be pushed: git commit -m "includes s47 reactjs Activity 5"
		-push your updates to your online repo: git push origin master

	Go to Boodle:
		-copy the url of the home page for your react-booking repo (URL on browser not the URL from clone button) and link it to boodle:

		WDC028-47 | React.js - Routing and Conditional Rendering

Activity:

Add a useEffect in our Register page to validate and check our inputs as the user types into our input.

	-Check if all states are not empty and check if the length of the mobileNum is equal to 11 characters and check if the password and confirm passwords match.
		-if they do, set the isActive state to true,
		-else, set it to false.

Check if there is alredy a logged in user by unwrapping our userContext and getting our user global state.
	-Add a ternary if, there is a user.id, redirect the user using the Navigate component. Redirect the logged in user back to the courses page.
	-Else, show the register form.


	Pushing Instructions:

	Go to Gitbash:
		-go to your b152/react-booking folder.
		-add your updates to be committed: git add .
		-commit your changes to be pushed: git commit -m "includes s48 reactjs Activity 6"
		-push your updates to your online repo: git push origin master

	Go to Boodle:
		-copy the url of the home page for your react-booking repo (URL on browser not the URL from clone button) and link it to boodle:

		WDC028-48 | React.js - State Management

Activity:

Create a fetch() in enroll() in the ViewCourse() component which will allow us to enroll to our current course:
	-Provide the correct Request URL
	-Provide the appropriate Method
	-Provide the token
	-Provide the proper headers.
	-Provide the appropriate request body.
		fetch for enroll:
			needs the courseId to be passed in the body.
			to pass the token as part of the Authorization headers.

Then, process the incoming response with res.json()
Then, console.log() the incoming data.


Pushing Instructions:

Go to Gitbash:
	-go to your b152/react-booking folder.
	-add your updates to be committed: git add .
	-commit your changes to be pushed: git commit -m "includes s49 reactjs Activity 7"
	-push your updates to your online repo: git push origin master

Go to Boodle:
	-copy the url of the home page for your react-booking repo (URL on browser not the URL from clone button) and link it to boodle:
 
	WDC028-49 | React.js - API Integration with Fetch





Activity:

Admin Dashboard Code-Along

Pushing Instructions:

Go to Gitbash:
	-go to your b152/react-booking folder.
	-add your updates to be committed: git add .
	-commit your changes to be pushed: git commit -m "includes s50 reactjs Activity 8"
	-push your updates to your online repo: git push origin master

Go to Boodle:
	-copy the url of the home page for your react-booking repo (URL on browser not the URL from clone button) and link it to boodle:

	WDC028-50 | React.js - API Integration with Fetch 2


